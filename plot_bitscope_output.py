
# Tinkerish hackery licensed under the terms of the GPL v.3
# Author: Michael Abel

# Try to reverse engineer a bitscope micro recording in order to reverse
# engineer a uart transmission

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv
import sys

if len(sys.argv) !=2:
    print('Usage: python3 plot_bitscope_output.py <file>.csv')

csvfile = sys.argv[1]

it = open(csvfile)

myreader = csv.reader(it, delimiter=',')

# First line is the header:
header = next(myreader)
print('Header: ',header, len(header))

s = np.array([])

header_data = None
print_header = True
for row in myreader:
    floats = map(float, row[9:])
    s = np.append(s, list(floats))

    if print_header:
        print('First Line: ', end='')
        print(row[0:9])
        header_data = row[0:9]
        print_header = False

print( 'Sample Width: ', 1/float(header_data[7]))
print( 'Sample Frequency: ', header_data[7])


print('Data length', len(s))
t = np.arange(0, len(s))

fig, ax = plt.subplots()

ax.plot(t, s)

ax.set(xlabel='samples', ylabel='voltage',
       title='Bitscope DSO')
ax.grid()


plt.show()

