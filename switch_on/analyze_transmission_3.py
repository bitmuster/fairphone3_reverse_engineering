
# Tinkerish, diletantish hackery licensed under the terms of the GPL v.3
# Author: Michael Abel

# Try to reverse engineer a bitscope micro recording in order to reverse
# engineer a uart transmission

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv

csvfile = 'BitScope_switch_on.csv'


#raw:
#trigger,stamp,channel,index,type,delay,factor,rate,count,data
#13346,20:33:26,0,13303,0,8E-7,2,8.42E5,1684,1.7469,1.7521,1.7567,1.7524,1.7469

#trigger, stamp,    channel, index, type, delay, factor, rate,   count, data
#13346,   20:33:26, 0,       13303, 0,    8E-7,  2,      8.42E5, 1684,  1.7469,1.7521,1.7567,1.7524,1.7469

# delay:
# >>> d = 8e-7
# >>> 1/d
# 1250000.0 -> 1.25 MHz sample rate (correct ???)

# rate :  8.42E ???

it = open(csvfile)

myreader = csv.reader(it, delimiter=',')

header = next(myreader)

print('Header: ',header, len(header))

s = np.array([])

for row in myreader:
	floats = map(float, row[9:])
	s = np.append(s, list(floats))

transmision_1_begin = 58955
transmision_1_end = 60623
s= s[transmision_1_begin : transmision_1_end]

# shift upwards
s = s + 1.8 # For whatever weird reason the voltage is negative

print('Data len', len(s))
t = np.arange(0, len(s))

fig, ax = plt.subplots()

#Original signal
#ax.plot(t, s)

ax.set(xlabel='samples', ylabel='voltage',
       title='Fairphone')
ax.grid()

# Nice rounded signal
# s2 = s.round()
# ax.plot(t, s2)

# Nice clarified and normed signal
#s3 = ( np.sign(s-0.9) + 1 ) * 0.5
#ax.plot(t, s3)

def identify(tstart, h= 0.2, twidth = 7.17):
    """Assumed 8n1"""
    tx = np.arange(0, twidth*11, twidth) + tstart
    #ty = [  h ] *11
    d = 0.02
    ty = [  h+d, h+d,
            h, h, h, h+d,
            h, h, h, h+d,
            h + d]
    ax.plot(tx, ty, '*' )

#identify(0)
#identify(80, 0.4, 7.25)
#identify(154, 0.2, 7.25)
#identify(227, 0.4, 7.25)
# Manual: 0x82 0x76 0x26 0x4E
#identify(300, 0.2, 7.25)
#identify(374, 0.4, 7.25)
#identify(447, 0.2, 7.25)
#identify(520, 0.4, 7.25)
# Manual: 0xF6 0x96 0x26 0x04

#plt.show()

def timing(v):
    if v >= 5 and v <= 9:
        return 1
    elif v >= 13 and v <= 17:
        return 2
    elif v >= 20 and v <= 22:
        return 3
    elif v >= 27 and v <= 29:
        return 4
    elif v >= 35 and v <= 36:
        return 5
    elif v >= 42 and v <= 43:
        return 6
    else:
        print('Oh %i'%v)

bitlength = 0
currentbit = 0 # read from the plot
tansmisstion_bits = []
the_transmission = []

for stuff in range(len(s)):
    #print(s[stuff])
    if currentbit == 1:
        if s[stuff] > 0.9:
            bitlength += 1
        else:
            currentbit = 0
            #print(bitlength)
            tansmisstion_bits.append(bitlength)
            the_transmission +=  [1]*timing(bitlength)
            bitlength = 0
    else:
        if s[stuff] <= 0.9:
            bitlength += 1
        else:
            currentbit = 1
            tansmisstion_bits.append(bitlength)
            the_transmission +=  [0]*timing(bitlength)
            bitlength = 0

bits = np.array(tansmisstion_bits)

print('max ', max(bits))
n, bins, patches = plt.hist(bits, range(50), facecolor='blue', alpha=0.5)
plt.show()

#>>> 43/6.0
#7.166666666666667
bit_distance = 7.166666666666667 # haha probably

# not really wrong
print(the_transmission, len(the_transmission))
# [
#0, 1, 0, 1, 0, 1, 1, 0, 1, 1, 
#0, 1, 0, 1, 0, 0, 0, 0, 1, 1, 
#0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 
#0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 
#0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 
#0, 1, 0, 0, 1, 0, 0, 1, 1, 1,
#0, 1, 0, 0, 1, 1, 0, 1, 1, 0, ups
#0, 1, 0, 0, 0, 1, 0, 1, 1, 0, ups
#0, 1, 0, 0, 1, 0, 1, 1, 1, 0, ups
#0, 1, 0, 0, 0, 0, 0, 0, 1, 0, ups
#0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 
#0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 
#0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 
#0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 
#0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 
#0, 1, 0, 0, 0, 1, 1, 0, 1, 1, 
#0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 
#0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 
#0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 
#0, 1, 0, 1, 0, 1, 0, 0, 1, 1, 
#0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 
#0, 1, 0, 0, 1, 0, 1, 1, 1, 0, ups
#0, 1, 0, 0, 0, 0, 0, 0] 228



t= the_transmission +[1] # first bit is idle; last stop bit was missing

assert ((len(t)%10) ==0)

print('Data len', len(t)/10)

h=[]
 
for i in range(len(t)//10):
	#print(i)
	assert( t[i*10] ==0) # start bit
	assert( t[i*10+9] ==1) # stop bit

	data = 0
	for k in range(8):
		data += t[i*10+k] * 2**(8-k)
	print(hex(data), ' ', end='')
	h.append(data)
print()

#for w in h:
#	print(chr(w), ' ', end='')
# random stuff

