
# Tinkerish hackery licensed under the terms of the GPL v.3
# Author: Michael Abel

# Try to reverse engineer a bitscope micro recording in order to reverse
# engineer a uart transmission

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv

csvfile = 'BitScope_switch_on.csv'


#raw:
#trigger,stamp,channel,index,type,delay,factor,rate,count,data
#13346,20:33:26,0,13303,0,8E-7,2,8.42E5,1684,1.7469,1.7521,1.7567,1.7524,1.7469

#trigger, stamp,    channel, index, type, delay, factor, rate,   count, data
#13346,   20:33:26, 0,       13303, 0,    8E-7,  2,      8.42E5, 1684,  1.7469,1.7521,1.7567,1.7524,1.7469

# delay:
# >>> d = 8e-7
# >>> 1/d
# 1250000.0 -> 1.25 MHz sample rate (correct ???)

# rate :  8.42E ???

it = open(csvfile)

myreader = csv.reader(it, delimiter=',')

header = next(myreader)

print('Header: ',header, len(header))

s = np.array([])

for row in myreader:
	floats = map(float, row[9:])
	s = np.append(s, list(floats))

print('Data len', len(s))
t = np.arange(0, len(s))

fig, ax = plt.subplots()

ax.plot(t, s)

ax.set(xlabel='samples', ylabel='voltage',
       title='Fairphone')
ax.grid()


plt.show()

