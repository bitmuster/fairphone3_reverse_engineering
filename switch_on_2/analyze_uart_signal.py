
# Tinkerish, diletantish hackery licensed under the terms of the GPL v.3
# Author: Michael Abel

# Try to reverse engineer a bitscope micro recording in order to reverse
# engineer a uart transmission

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import csv
import sys

if len(sys.argv) !=2:
    print('Usage: python3 plot_bitscope_output.py <file>.csv')

csvfile = sys.argv[1]
it = open(csvfile)
myreader = csv.reader(it, delimiter=',')

header = next(myreader)

print('Header: ',header, len(header))

s = np.array([])

for row in myreader:
    floats = map(float, row[9:])
    s = np.append(s, list(floats))

# First part
begin = 210011
end = 212000
print('Analyzing samples %i ... %i'%(begin,end))
s= s[begin:end]

# Second part : currently 113 bits
#begin = 214017
#end = 216000
#print('Analyzing samples %i ... %i'%(begin,end))
#s= s[begin:end]

# shift upwards
s = s + 1.8 # For whatever weird reason the voltage is negative

print('Data len', len(s))
t = np.arange(0, len(s))

fig, ax = plt.subplots()

#Original signal
ax.plot(t, s)

ax.set(xlabel='samples', ylabel='voltage',
       title='Fairphone')
ax.grid()

# Nice rounded signal
s2 = s.round()
ax.plot(t, s2)

# Nice clarified and normed signal
# Should even norm the weird signal here to 0 and 1
s3 = ( np.sign(s-0.9) + 1 ) * 0.5
ax.plot(t, s3)

plt.show()


# Critical value: 79 4.5578 4.6
# Critical value: 2017 116.3676 116.4
# Todo: This is between both measured transmissions
def timing(v):
    bit_distance = 17.333
    print(v, round(v/bit_distance,4) , round(v/bit_distance,1))
    return round(v/bit_distance)
    
bitlength = 0
currentbit = 0 # read from the plot
tansmisstion_bits = []
the_transmission = []

# We assume only 0 and 1 as valid values in array s


signal = s3

for stuff in range(len(signal)):
    if currentbit == 1:
        if signal[stuff] == 1:
            bitlength += 1
        else:
            currentbit = 0
            #print(bitlength)
            tansmisstion_bits.append(bitlength)
            the_transmission +=  [1]*timing(bitlength)
            bitlength = 0
    else:
        if signal[stuff] == 0:
            bitlength += 1
        else:
            currentbit = 1
            tansmisstion_bits.append(bitlength)
            the_transmission +=  [0]*timing(bitlength)
            bitlength = 0
    

bits = np.array(tansmisstion_bits)

print('max ', max(bits))
n, bins, patches = plt.hist(bits, range(200), facecolor='blue', alpha=0.5)
plt.show()

#>>> 104/6 = 17.333333333333332
#>>> 51.4/3 = 17.133333333333333
bit_distance = 17.333

t= the_transmission

print('Data length (bits) ', len(t))
print('Data length (bytes) ', len(t)/10)
assert ((len(t)%10) ==0)

h=[]
 
for i in range(len(t)//10):
    #print(i)
    assert( t[i*10] ==0) # start bit
    assert( t[i*10+9] ==1) # stop bit

    data = 0
    for k in range(8):
        data += t[i*10+k+1] * 2**(k)
    print(hex(data), ' ', end='')
    h.append(data)
print()

for w in h:
    print(str(chr(w)), ' ', end='')
# random stuff

#First part:
#0x69  0x6f  0x6e  0x20  0x27  0x5b  0x33  0x34  0x30  0x30  0x5d
#i  o  n     '  [  3  4  0  0  ]

