
Fairphone 3 Reverse Engineering
===============================

Various attempts to reverse engineer uart signals within a Fairphone 3.

Folder "switch_on" contains signals traced from pin T to GND.


See here for pin assignments:

https://github.com/k65onyx/fp3-notes

